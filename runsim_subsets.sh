#!/bin/bash
# Remove old command list
rm commands.txt

# Create new command list with all combinations of dataset, package, iteration, case proportion, spike method and effect size (spike-in magnitude). 
# Subset this as desired, will create several hundred thousand lines if left unchanged. (9 * 9 * 150 * 3 * (1 + 5 + 4 + 1) = 400950)
# The resulting lines consist of Rscript calls, which is then parallelized using GNU parallel.

for datasetname in otu_table_fyr_small otu_table_fyr_medium otu_table_w36_small otu_table_w36_medium otu_table_ab_small otu_table_ab_medium otu_table_fyr otu_table_w36 otu_table_ab  # New datasets can be added/substituted here. Will be loaded from data/NAME.RData as described in the tutorial
do
	for packageName in des edg bay mgs pra glm tte ltt wil # New methods can be added/substituted here. Will be sourced from lib/NAME.R as described in the tutorial
	do
		for i in {1..150}   # Set number of iterations
		do
			for caseProp in 50 25 10  # Set case proportions
			do
				spikeMethod="none"  # For False positive rate tests
				effectSize=1
					echo "echo Running pkg $packageName - ES $effectSize - prop $caseProp - no. $i - method $spikeMethod on data $datasetname" >> commands.txt
					echo "Rscript DASim.R $packageName $caseProp $effectSize $i $spikeMethod $datasetname" >> commands.txt

				spikeMethod="mult"  # For multiplicative spike-in tests
				for effectSize in 0.5 2 5 10 20  # Set desired effect sizes / spike in magnitudes
				do
					echo "echo Running pkg $packageName - ES $effectSize - prop $caseProp - no. $i - method $spikeMethod on data $datasetname" >> commands.txt
					echo "Rscript DASim.R $packageName $caseProp $effectSize $i $spikeMethod $datasetname" >> commands.txt
				done
				spikeMethod="add"  # For additive spike-in tests
				for effectSize in 0.5 2 5 10 # Set desired effect sizes / spike in magnitudes
				do
					echo "echo Running pkg $packageName - ES $effectSize - prop $caseProp - no. $i - method $spikeMethod on data $datasetname" >> commands.txt
					echo "Rscript DASim.R $packageName $caseProp $effectSize $i $spikeMethod $datasetname" >> commands.txt
				done
				spikeMethod="mix"  # A multiplicative spike-in method with multiple effect sizes
				effectSize=0  # A dummy effect size only used with this mixed spike-in
					echo "echo Running pkg $packageName - ES $effectSize - prop $caseProp - no. $i - method $spikeMethod on data $datasetname" >> commands.txt
					echo "Rscript DASim.R $packageName $caseProp $effectSize $i $spikeMethod $datasetname" >> commands.txt
			done
		done
	done
done

# Execute commands using GNU parallel with the desired number of parallel cores.
cat commands.txt | parallel --jobs 70
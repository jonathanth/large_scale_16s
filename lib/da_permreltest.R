run_permreltest <- function(otu_table, outcome, noOfIterations = 10000, seed = as.numeric(Sys.time()), testName = "", margin = 50){
  # otu_table
  # outcome
  # spiked_otus
  
  otu_table <- apply(otu_table, 2, function(x) x/sum(x))
  
  set.seed(seed)
  nullStatList <- list()
  
  message("Running COPSAC permtest (relative abundances), ",testName, " - ",noOfIterations, "iterations, ", ncol(otu_table), " samples, ", nrow(otu_table), " OTUs.")

  # Create shuffled outcomes
  message(format(Sys.time(), "%H:%M:%S"), " - Shuffling outcomes...")
  shuffledOutcomesList <- list()
  for (k in 1:noOfIterations){
    shuffledOutcomesList[[k]] <- sample(outcome)
    if(k %in% (noOfIterations*c(0.1,1,0.5,1:10)/10))
      message(format(Sys.time(), "%H:%M:%S")," - ", round(k/noOfIterations,2)*100, "% complete with shuffle.")
  }
  
  message(format(Sys.time(), "%H:%M:%S")," - Shuffle complete, testing...")

  iterations <- nrow(otu_table)
  p <- numeric(iterations)
  FC <- numeric(iterations)
  coverage <- numeric(iterations)

  for (i in 1:iterations){
    message(format(Sys.time(), "%H:%M:%S"), " - Running ", testName, ": OTU ",i,"/",iterations," ~ ", round(i/iterations*100,2),"% complete...")
    otu_row      <- as.numeric(otu_table[i,])
  
    real_case    <- mean(otu_row[outcome==1])
    real_control <- mean(otu_row[outcome==0])
    realStat     <- log((real_case+1)/(real_control+1))^2
  
    # nullStat   <- sapply(shuffledOutcomesList, FUN=function(x){
    Wnull <- numeric(noOfIterations)

    for(j in 1:noOfIterations){
      case     <- mean(otu_row[shuffledOutcomesList[[j]]==1])
      control  <- mean(otu_row[shuffledOutcomesList[[j]]==0])
      Wnull[j] <- log((case+1)/(control+1))^2 # test statistic

      if(j %in% 10^(1:100)){
        nullStatTemp <- Wnull[1:j]
        ptemp <- mean(nullStatTemp >= realStat)
        if(ptemp > (margin*(1/j))){
          nullStat <- Wnull[1:j]
          coverage[i] <- j
          break
        }
      }
      if(j == noOfIterations){
         coverage[i] <- j
         nullStat <- Wnull
      }
    }
  
    p[i]         <- mean(nullStat >= realStat)
    if(p[i] == 0){ #only at level of support
      p[i] <- 1/noOfIterations
    }
    FC[i]        <- mean(otu_row[outcome == 1]) / mean(otu_row[outcome == 0])
  }
  output_df <- data.frame(OTU = row.names(otu_table), pval = p, FC, coverage)
  output_df 
}